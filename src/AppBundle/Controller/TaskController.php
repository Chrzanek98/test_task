<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Task;
use AppBundle\Form\Handler\TaskFormHandler;
use AppBundle\Form\TaskForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TaskController extends Controller
{
    /**
     * @Route("/task/list/{status}", name="task_list", condition="request.isXmlHttpRequest()", methods={"GET"})
     * @param $status
     * @return Response
     */
    public function listAction($status = Task::STATUS_IN_PROGRESS)
    {
        $manager = $this->getDoctrine()->getManager();
        $query = 'SELECT task.id as task_id, task.name as task_name, category.name as category_name FROM task
                  INNER JOIN category on task.category_id = category.id WHERE task.status = :status';

        $statement = $manager->getConnection()->prepare($query);
        $statement->bindValue('status', $status);
        $statement->execute();

        $template = $status === Task::STATUS_IN_PROGRESS ? 'list' : 'doneList';

        return $this->render('task/'.$template.'.html.twig', [
            'results' => $statement->fetchAll()
        ]);
    }

    /**
     * @Route("/task/{id}/remove", name="task_remove", condition="request.isXmlHttpRequest()", methods={"DELETE"})
     * @param $id int
     * @return JsonResponse
     */
    public function removeAction($id)
    {
        $manager = $this->getDoctrine()->getManager();
        $query = 'DELETE FROM task WHERE task.id = :id';

        $statement = $manager->getConnection()->prepare($query);
        $statement->bindValue('id', $id);
        $statement->execute();

        return new JsonResponse(['success' => 1], Response::HTTP_OK);
    }


    /**
     * @Route("/task/{id}/mark/done", name="task_mark_as_done", condition="request.isXmlHttpRequest()", methods={"PUT"})
     * @param $id int
     * @return JsonResponse
     */
    public function markAsDoneAction($id)
    {
        $manager = $this->getDoctrine()->getManager();
        $query = 'UPDATE task SET task.status = :status WHERE task.id = :id';

        $statement = $manager->getConnection()->prepare($query);
        $statement->bindValue('status', Task::STATUS_DONE);
        $statement->bindValue('id', $id);
        $statement->execute();

        return new JsonResponse(['success' => 1], Response::HTTP_OK);
    }

    /**
     * @Route("/task/{id}/edit", name="task_edit", methods={"GET", "POST"})
     * @param $id int
     * @param TaskFormHandler $formHandler
     * @param Request $request
     * @return Response
     */
    public function editAction($id, TaskFormHandler $formHandler, Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Task');
        $task = $repository->findOneBy(['id' => $id]);

        $form = $this->createForm(TaskForm::class, $task);

        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $formHandler->process($form->getData());
            return $this->redirectToRoute('homepage');
        }

        return $this->render('task/edit.html.twig', [
            'form' => $form->createView(),
            'id' => $id
        ]);
    }
}
