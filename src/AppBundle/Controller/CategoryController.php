<?php

namespace AppBundle\Controller;

use AppBundle\Form\CategoryForm;
use AppBundle\Form\Handler\CategoryFormHandler;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends Controller
{
    /**
     * @Route("/category/list", name="category_list", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function listAction()
    {
        $manager = $this->getDoctrine()->getManager();
        $query = 'SELECT id, name FROM category';

        $statement = $manager->getConnection()->prepare($query);
        $statement->execute();

        return $this->render('category/list.html.twig', [
            'results' => $statement->fetchAll()
        ]);
    }

    /**
     * @Route("/category/form", name="category_form")
     * @param $request Request
     * @param $formHandler CategoryFormHandler
     * @return Response
     */
    public function formAction(Request $request, CategoryFormHandler $formHandler)
    {
        $form = $this->createForm(CategoryForm::class);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $formHandler->process($form->getData());
            return $this->redirectToRoute('homepage');
        }

        return $this->render('category/form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/category/{id}/remove", name="category_remove", condition="request.isXmlHttpRequest()", methods={"DELETE"})
     * @param $id int
     * @return JsonResponse
     */
    public function removeAction($id)
    {
        $manager = $this->getDoctrine()->getManager();
        $query = 'DELETE FROM task WHERE category_id = :id;DELETE FROM category WHERE category.id = :id';

        $statement = $manager->getConnection()->prepare($query);
        $statement->bindValue('id', $id);
        $statement->execute();

        return new JsonResponse(['success' => 1], Response::HTTP_OK);
    }
}
