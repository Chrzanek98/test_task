<?php

namespace AppBundle\Controller;

use AppBundle\Form\Handler\TaskFormHandler;
use AppBundle\Form\TaskForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @param TaskFormHandler $formHandler
     * @return Response
     */
    public function indexAction(Request $request, TaskFormHandler $formHandler)
    {
        $taskForm = $this->createForm(TaskForm::class);
        $taskForm->handleRequest($request);

        if ($taskForm->isValid() && $taskForm->isSubmitted()) {
            $formHandler->process($taskForm->getData());
            unset($taskForm);
            $taskForm = $this->createForm(TaskForm::class);
        }

        return $this->render('default/index.html.twig', [
            'taskForm' => $taskForm->createView()
        ]);
    }
}
