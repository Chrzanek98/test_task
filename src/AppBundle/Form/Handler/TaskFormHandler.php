<?php

namespace AppBundle\Form\Handler;

use AppBundle\Entity\Task;
use Doctrine\ORM\EntityManagerInterface;

class TaskFormHandler
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function process(Task $task)
    {
        $this->entityManager->persist($task);
        $this->entityManager->flush();
    }
}