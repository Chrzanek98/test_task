<?php

namespace AppBundle\Form\Handler;

use AppBundle\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;

class CategoryFormHandler
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function process(Category $category)
    {
        $this->entityManager->persist($category);
        $this->entityManager->flush();
    }
}